#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_sdlrenderer.h>
#include <SDL.h>

#if !SDL_VERSION_ATLEAST(2,0,17)
#error This backend requires SDL 2.0.17+ because of SDL_RenderGeometry() function
#endif

#include "types.h"

typedef unsigned short int word;

struct Memory {
  // TODO:
  //   this is probably oversized,
  //   when tried with smaller, the prog segfaulted
  static const size_t ram_sz = 0x30000;
  static const size_t screen_offset = 16000;
  static const size_t screen_col_n = 512;
  static const size_t screen_row_n = 256;
  static const size_t pixel_per_word = 16;
  static const size_t keyboard_offset = 24000;
  static const size_t screen_sz = keyboard_offset - screen_offset;

  word ram[ram_sz];
  word *screen = &ram[screen_offset];
  word *keyboard = &ram[24000];

} Memory;

typedef struct WordStack {
  word **array;
  size_t i;

} WordStack;

WordStack create_stack(size_t size) {
  WordStack stack;
  stack.i = 0;
  stack.array = (word **)malloc(sizeof(word *) * size);
  if (stack.array == NULL) {
    fprintf(stderr, "Couldn't malloc for WordStack, szeva\n");
    exit(1);
  }
  return stack;
}

word *get_pixel(size_t x, size_t y) {
  size_t x_rel = x / Memory.pixel_per_word;
  return &Memory.screen[y * Memory.screen_col_n + x_rel];
}

bool read_pixel(size_t x, size_t y) {
  size_t remainder = x % Memory.pixel_per_word;
  word *selected_word = get_pixel(x, y);
  bool val = (*selected_word >> remainder) & 1U;
  return val;
}

void write_pixel(size_t x, size_t y, bool val) {
  size_t remainder = x % Memory.pixel_per_word;
  word *selected_word = get_pixel(x, y);
  // https://stackoverflow.com/a/47990
  if(val) {
    *selected_word |= 1UL << remainder;
  } else {
    *selected_word &= ~(1UL << remainder);
  }
}

void generate_checkboard(void) {
  // generate chessboard
  for(size_t row = 0; row < Memory.screen_row_n; ++row) {
    for(size_t col = 0; col < Memory.screen_col_n; ++col) {
      bool val = (row + col) % 2 ? true : false;
      write_pixel(col, row, val);
    }
  }
}

int main(int argc, char *argv[]) {

  Stack<word> kill_stack = Stack<word>(Memory.screen_sz);
  Stack<word> birth_stack = Stack<word>(Memory.screen_sz);

  if (SDL_Init(SDL_INIT_VIDEO
	       | SDL_INIT_TIMER
	       | SDL_INIT_GAMECONTROLLER) != 0) {
    fprintf(stderr, "Error: %s\n", SDL_GetError());
    return -1;
  }

  SDL_WindowFlags window_flags =
    (SDL_WindowFlags)(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
  SDL_Window* window =
    SDL_CreateWindow("Chack-the-hack-VM",
		     SDL_WINDOWPOS_CENTERED,
		     SDL_WINDOWPOS_CENTERED,
		     1280, 720,
		     window_flags);
  SDL_Renderer* renderer =
    SDL_CreateRenderer(window, -1,
		       SDL_RENDERER_PRESENTVSYNC
		       | SDL_RENDERER_ACCELERATED);

  if (renderer == NULL) {
    SDL_Log("Error creating SDL_Renderer!");
    return 0;
  }


  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;

  ImGui::StyleColorsDark();
  ImGui_ImplSDL2_InitForSDLRenderer(window, renderer);
  ImGui_ImplSDLRenderer_Init(renderer);
  
  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
  ImVec4 draw_color_black = ImVec4(0.0f, 0.0f, 0.0f, 1.00f);
  ImVec4 draw_color_red = ImVec4(1.0f, 0.0f, 0.0f, 1.00f);

  generate_checkboard();
  // main loop
  for(;;) {

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      ImGui_ImplSDL2_ProcessEvent(&event);
      if (event.type == SDL_QUIT)
	goto end;
      if (event.type == SDL_WINDOWEVENT
	  && event.window.event == SDL_WINDOWEVENT_CLOSE
	  && event.window.windowID == SDL_GetWindowID(window))
	goto end;
    }


    // start frame
    ImGui_ImplSDLRenderer_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();


    // FPS
 
    ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();
 
    // Rendering
    ImGui::Render();
    SDL_SetRenderDrawColor(renderer, (Uint8)(clear_color.x * 255), (Uint8)(clear_color.y * 255), (Uint8)(clear_color.z * 255), (Uint8)(clear_color.w * 255));
    SDL_RenderClear(renderer);
    ImGui_ImplSDLRenderer_RenderDrawData(ImGui::GetDrawData());

    // my render
    

    // draw rects;
    // TODO: this could be easily optimized.
    size_t rect_width = 2;
    size_t rect_height = rect_width;
    size_t padding = 0;
    SDL_Rect rects[Memory.screen_sz * Memory.pixel_per_word] = {0};

    for(size_t row = 0; row < Memory.screen_row_n; ++row) {
      for(size_t col = 0; col < Memory.screen_col_n; ++col) {
	SDL_Rect *rect = &rects[row * Memory.screen_row_n + col];
	rect->x = (rect_width + padding) * col; 
	rect->y = (rect_height + padding) * row;
	rect->w = rect_width;
	rect->h = rect_height;
	if(read_pixel(col, row)) {
	  SDL_SetRenderDrawColor(renderer, (Uint8)(draw_color_black.x * 255), (Uint8)(draw_color_black.y * 255), (Uint8)(draw_color_black.z * 255), (Uint8)(clear_color.w * 255));
	  SDL_RenderFillRect(renderer, rect);
	} else {
	  SDL_SetRenderDrawColor(renderer, (Uint8)(draw_color_red.x * 255), (Uint8)(draw_color_red.y * 255), (Uint8)(draw_color_red.z * 255), (Uint8)(clear_color.w * 255));
	  SDL_RenderFillRect(renderer, rect);
	}
      }
    }
    // END my render
    SDL_RenderPresent(renderer);
  }

 end:
  return 0;
}
