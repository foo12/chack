template <class T>
class Stack {
  T *stack;
  size_t sp;

public:
  Stack(size_t sz) {
    this->stack = (T *)malloc(sizeof(T) * sz);
  }

  void reset(void) {
    sp = 0;
  }

  void push(T *word) {
    stack[sp] = word;
    sp++;
  }

  T *pop(void) {
    sp--;
    return &stack[sp];
  }
};
