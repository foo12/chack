EXE = main
SOURCES = src/main.cpp

# https://github.com/ocornut/imgui/blob/master/examples/example_sdl_sdlrenderer/Makefile
IMGUI_DIR = lib/imgui

# imgui
SOURCES += $(IMGUI_DIR)/imgui.cpp $(IMGUI_DIR)/imgui_demo.cpp $(IMGUI_DIR)/imgui_draw.cpp $(IMGUI_DIR)/imgui_tables.cpp $(IMGUI_DIR)/imgui_widgets.cpp
SOURCES += $(IMGUI_DIR)/backends/imgui_impl_sdl.cpp $(IMGUI_DIR)/backends/imgui_impl_sdlrenderer.cpp
OBJS = $(addprefix obj/, $(addsuffix .o, $(basename $(notdir $(SOURCES)))))
UNAME_S := $(shell uname -s)

CXXFLAGS = -std=c++11 -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends
CXXFLAGS += -Wall -Wformat

# SDL2
SDL2_CFLAGS = `lib/SDL2/bin/sdl2-config --cflags`
SDL2_LIBS = `lib/SDL2/bin/sdl2-config --libs`

CXXFLAGS += $(SDL2_CFLAGS)
LIBS = $(SDL2_LIBS) -lGL -ldl

main: $(OBJS)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS) -g

release: $(OBJS)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS) -O2

clean:
	rm -f $(EXE) $(OBJS)

obj/%.o:src/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

obj/%.o:$(IMGUI_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

obj/%.o:$(IMGUI_DIR)/backends/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<
